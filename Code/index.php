<!DOCTYPE html>

<html>
<!--
<html manifest="cache-manifest.manifest">
-->
<head>
    <title>Petro-Trader</title>
    <!--
    /* @license
    * MyFonts Webfont Build ID 2326287, 2012-07-18T10:53:09-0400
    *
    * The fonts listed in this notice are subject to the End User License
    * Agreement(s) entered into by the website owner. All other parties are
    * explicitly restricted from using the Licensed Webfonts(s).
    *
    * You may obtain a valid license at the URLs below.
    *
    * Webfont: Slaphappy by Comicraft
    * URL: http://www.myfonts.com/fonts/comicraft/slaphappy/regular/
    * Copyright: Copyright (c) Active Images, 2009. All rights reserved.
    * Licensed pageviews: 10,000
    *
    * Webfont: Slaphappy Dropcaps by Comicraft
    * URL: http://www.myfonts.com/fonts/comicraft/slaphappy/dropcaps/
    * Copyright: Copyright (c) Active Images, 2009. All rights reserved.
    * Licensed pageviews: unspecified
    *
    * Webfont: Slaphappy Open by Comicraft
    * URL: http://www.myfonts.com/fonts/comicraft/slaphappy/open/
    * Copyright: Copyright (c) ActiveImages, 2009. All rights reserved.
    * Licensed pageviews: unspecified
    *
    *
    * License: http://www.myfonts.com/viewlicense?type=web&buildid=2326287
    *
    * © 2012 Bitstream Inc
    */

    -->
    <link rel="stylesheet" type="text/css" href="css/MyFontsWebfontsKit.css">
    <link rel="stylesheet" type="text/css" media="screen" href="css/main.css" >
    <link rel="stylesheet" type="text/css" media="screen" href="css/petrotrader.css" >

    <!--
    <base href="http://webcal.ikosta.de/">
    -->
    <meta charset="utf-8">
    <!--
    <link rel="apple-touch-icon" href="img/touch-icon-iphone.png">
    <link rel="apple-touch-icon" sizes="72x72" href="img/touch-icon-ipad.png">
    <link rel="apple-touch-icon" sizes="114x114" href="img/touch-icon-iphone4.png">
    <link rel="apple-touch-startup-image" href="img/touch-startup.png">
    -->
    <!-- startup image for web apps - iPad - landscape (1024x748) -->
    <link rel="apple-touch-startup-image" href="img/touch-startup-ipad-landscape.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape)" />

    <!-- startup image for web apps - iPad - portrait (768x1024) -->
    <link rel="apple-touch-startup-image" href="img/touch-startup-ipad-portrait.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait)" />
    <link rel="apple-touch-icon" sizes="72x72" href="img/touch-icon-ipad.png">

    <!-- This meta tag ensu res that the toolbar at the bottom of the browser
		is hidden when this page is accessed frome the Home Screen. -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="robots" content="noindex, nofollow">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"  />
    <!--
    <meta content="minimum-scale=1.0, width=device-width, maximum-scale=0.6667, user-scalable=no" name="viewport">
    -->

    <script type="text/javascript" src="lib/jquery-1.7.1.min.js" charset="utf-8"></script>
    <script type="text/javascript" src="lib/soundmanager2-nodebug-jsmin.js"></script>
    <!--
        Replace this for deployment with min version

    <script type="text/javascript" src="lib/impact/impact.js"></script>
    <script type="text/javascript" src="lib/game/main.js"></script>
     -->
    <script type="text/javascript" src="lib/game.min.js" ></script>

    <script>
        // block scrolling on iOS devices
        document.addEventListener('touchmove', function(e){ e.preventDefault(); });
        // preload web image
        $( document ).bind('ready', function(e) { $('#preloader').removeClass();})
        /*
        $( document ).bind( "mobileinit", function(){
            $.extend($.mobile, {
                loadingMessage: "loading …",
                minScrollBack: '50'
            });
            $.mobile.page.prototype.options.degradeInputs.date = true;
            $.mobile.selectmenu.prototype.options.nativeMenu = false;
            $.mobile.page.prototype.options.addBackBtn = true;
        });  */

        // game setup definition with offline fallbacks
        setup = {
            host: '<?php echo ( !empty( $_SERVER["SERVER_NAME"] ) ) ? $_SERVER["SERVER_NAME"] : localhost; ?>',
            userID: <?php echo ( !empty( $_GET["user_id"] ) ) ? $_GET["user_id"] : -1; ?>,
            hash: '<?php echo ( !empty( $_GET["hash"] ) ) ? $_GET["hash"] : ''; ?>',
            gameID: <?php echo ( !empty( $_GET["game_id"] ) ) ? $_GET["game_id"] : -1; ?>,
            lang: '<?php echo ( !empty( $_GET["lang"] ) ) ? $_GET["lang"] : 'de'; ?>',
            highScores: [<?php echo ( !empty( $_GET["high_scores"] ) ) ?  $_GET["high_scores"] : '-1'; ?>]
        }

        // DOCUMENT
        soundManager.audioFormats.ogg.required = true;
        soundManager.defaultOptions.autoLoad = true;
        soundManager.setup({
            // location: path to SWF files, as needed (SWF file name is appended later.)
            url: 'lib/swf/soundmanager2.swf',
            //flashVersion: 8, // optional: shiny features (default = 8)
            useFlashBlock: false,
            useHTML5Audio: true,
            preferFlash: false,
            waitForWindowLoad:true,

            onready: function() {},
            // optional: ontimeout() callback for handling start-up failure
            ontimeout: function() {}
        });

    </script>
</head>

<body>
    <div id="wrapper">
        <canvas id="canvas"></canvas>
        <!-- Overlay covering the canvas -->
        <div id="overlay" class="opaque"></div>

        <!-- introduction dialog -->
        <div id="splash" class="interactive">
            <img src="img/splash-screen_DE.jpg" alt="Petro-Trader Splash Screen" title="Petropia Petro-Trader"/>
        </div>

        <!-- introduction dialog -->
        <div id="introduction" class="dialog hidden">
            <div class="container">
                <p class="title">Spieleinleitung</p>
                <p class="content">
                    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
                </p>
                <div class="images">
                    <div class="illustration"> </div>
                </div>
                <div class="buttons">
                    <button class="return">ZUR&Uuml;CK</button>
                    <button class="continue">TEMP</button>
                </div>
            </div>
        </div>

        <!-- message dialog
        <div id="event" class="dialog hidden">
            <div class="container">
                <p class="title">Meldung</p>
                <p class="content">
                    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
                </p>
                <div class="images">
                    <div class="illustration"> </div>
                </div>
                <div class="buttons">
                    <button class="continue">OK</button>
                </div>
            </div>
        </div>
        -->
        <!-- alert dialog -->
        <div id="alert" class="dialog hidden">
            <div class="container">
                <p class="title">Hinweis</p>
                <p class="content">
                    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
                </p>
                <div class="images">
                    <div class="illustration"> </div>
                </div>
                <div class="buttons">
                    <button class="return">ABBRECHEN</button>
                    <button class="continue">BAUEN</button>
                </div>
            </div>
        </div>

        <!-- evaluation dialog -->
        <div id="evaluation" class="dialog hidden">
            <div class="container">
                <p class="title">Spielauswertung</p>
                <p class="content">
                    Du hast <span class="score">{SCORE}</span> Punkte erspielt.<br/><br/>Das ist <span class="comment">{COMMENT}</span> Punktzahl.
                </p>
                <div class="buttons">
                    <button class="retry">NOCHMAL</button>
                </div>
            </div>
        </div>

        <!-- Helper for preloading the webfonts and the websprite -->
        <div id="preloader" class="hidden">.</div>
    </div>
</body>
</html>
