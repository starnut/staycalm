ig.module(
    'game.config'
)
.requires(
    'game.scarestate'
)
.defines(function(){ //"use strict";

    Config = {

        LEVELS: [
            {
                name: 'level1',
                time: 300,              // sec
                funds: 2000
            }
        ],


        PLAYER: {
            SETTINGS: {
                moveSpeed: 100,
                runSpeed: 100,
                panicFactor: 1,
                panicLevel: 10,
                minPanicLevel: 5,
                currentScareState: Scarestate.INIT
            },
            LIGHT_RADIUS: 65
        },

        PANIC_FACTOR: {
            HIGH: 2.5,
            MEDIUM: 1.2,
            INIT: 0.3,
            LOW: -1.5
        },

        ITEMS: {
            key: {
                type: 'key',
                amount: 1
            },
            axe: {
                type: 'axe',
                amount: 1
            },
            doorKey: {
                type: 'doorKey',
                amount: 1
            },
            candles: {
                type: 'candles',
                amount: 6
            }
        },


        SOUND: {
            AXE: 'axe',
            CANDLE: 'candle',
            CRASH: 'crash',
            CLOSETOPEN: 'closetopen',
            DEATH: 'death',
            HEARTBEAT: 'Heartbeatsl',
            KEY: 'key',
            ZIP: 'zip'
        },

        playSoundFile: function (name)
        {
            Config.playSoundFile(name,null);
        },

        playSoundFile: function (name, volume)
        {
            //console.log('play sound = ' + name);
            var sound = new ig.Sound( 'media/sounds/'+name+'.*' );
            if (volume)
            {
                sound.volume = volume;
            }
            sound.play();
        },

        /**
         * Configuration of the petrol tank.
         */
        TANK: {
            POS: { x:342, y:576 },
            SETTINGS: {
                capacity: 2400,              // initial capacity in abstract units
                supply: 1200,                // initial supply in abstract units
                consumptionSpeed: 0.3,      // units consumed per frame
                refillSpeed: 1              // units refilled per frame
            }
        },

        UI: {

        },

        /*
         * Game setup. DON'T TOUCH!
         */

        SETUP: {
            FPS: 60,
            WIDTH: 1024,
            HEIGHT: 768,
            SCALE: 1
        },

        TILE: {
            WIDTH: 60,
            HEIGHT: 60
        }
    };
});