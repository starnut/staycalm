



ig.module(
    'game.entities.candle'
)
.requires(
    'impact.entity'
)
.defines(function (){

    EntityCandle = ig.Entity.extend({

        // Weltmeister - Map Editor
        _wmDrawBox: true,
        _wmBoxColor: 'rgba(255, 0, 0, 0.5)',

        size: {x: 42, y:123},
        fillStyle:'rgb( 255, 0, 0 )',
        burnDuration: 60,
        lightRadius: 120,
        currentRadius: 0,

        clippingOffset: {x:21, y:10},

        timer: null,
        isBurning: false,
        isBurnable: false,
        isNewTouch:true,

        // Load an animation sheet
        animSheet: new ig.AnimationSheet( 'media/entities/candle.png', 42, 123 ),

        init: function(x, y, settings) {
            this.addAnim( 'idle', 1, [0] );
            this.addAnim( 'burning', 0.2, [1,2,3] );
            this.parent(x, y, settings);
        },

        update: function() {

            if (this.timer)
            {
                //this.health = (this.burnDuration - this.timer.delta()) * 10;
                this.currentRadius = Math.round( this.lightRadius * ( this.burnDuration - this.timer.delta() ) / this.burnDuration );
                //this.currentRadius = ( this.currentRadius <= 0 ) ? 0 : this.currentRadius;
                if ( this.currentRadius <= 0 )
                {
                    this.currentRadius = 0;
                    this.timer.pause();
                    this.timer = null;
                    this.isBurning = false;
                    this.currentAnim = this.anims.idle;
                }
            }
            this.parent();
        },

//        draw: function() {
//            this.parent();
//        },

        refill: function() {
            this.isBurnable = true;
        },

        burn: function () {
            if ( this.isBurnable ) {
                if(!ig.game.isCandleIntroductionShown) {
                    ig.game.isCandleIntroductionShown = true;
                    ig.game.text.playTextBlock( ig.game.text.textSystem.eventCandleHolder);
                }

                this.isBurnable = false;
                this.isBurning = true;
                this.timer = new ig.Timer();
                this.timer.set(0);
                this.currentAnim = this.anims.burning.rewind();
                Config.playSoundFile(Config.SOUND.CANDLE);
            }
        }


    });


});