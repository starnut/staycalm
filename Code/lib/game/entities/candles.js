/**
 * Created by Michel Wacker on 26.01.13
 * Copyright by Starnut, all rights reserved.
 */
ig.module(
    'game.entities.candles'
)
.requires(
    'game.entities.internal.hotspot'
)
.defines(function(){

    introductionPlayed:false,

    EntityCandles = EntityHotspot.extend({
        size: { x: 64,  y: 64 },
        animSheet: new ig.AnimationSheet( 'media/entities/candles.png', 64, 64 ),

        init: function( x, y, settings ) {
            if ( this.animSheet ) {
                this.addAnim( 'idle', 0.01, [0,1,2] );
            }
            this.parent( x, y, settings );
        },

        activate: function() {
            this.visible = false;
            return this.parent();
        }
    });
});