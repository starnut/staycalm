/**
 * Created by Michel Wacker on 26.01.13
 * Copyright by Starnut, all rights reserved.
 */
ig.module(
    'game.entities.chest'
)
.requires(
    'game.entities.internal.hotspot'
)
.defines(function(){

    EntityChest = EntityHotspot.extend({
        size: { x: 56,  y: 53 },
        animSheet: new ig.AnimationSheet( 'media/entities/chest.png', 56, 53 )
    });
});