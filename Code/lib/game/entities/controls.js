ig.module(
    'game.entities.controls'
)
    .requires(
    'impact.entity'
)
    .defines(function (){

        EntityControls = ig.Entity.extend({

            // Load an animation sheet
            animSheet: new ig.AnimationSheet( 'media/entities/controls.png', 250, 211 ),

            init: function(x, y, settings) {
                this.addAnim( 'idle', 1, [0] );
                this.parent(x, y, settings);
                this.size = { x: 250,  y: 211};
                this.pos = { x : (ig.system.width-this.size.x-25), y : (ig.system.height-this.size.y-25) };
            }
        });
    });