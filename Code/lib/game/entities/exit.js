/**
 * Created by Michel Wacker on 26.01.13
 * Copyright by Starnut, all rights reserved.
 */
ig.module(
    'game.entities.exit'
)
.requires(
    'game.entities.internal.hotspot'
)
.defines(function(){

    EntityExit = EntityHotspot.extend({
        size: { x: 59,  y: 115 },
        animSheet: new ig.AnimationSheet( 'media/entities/exit.png', 59, 115 )
    });
});