ig.module(
    'game.entities.internal.hotspot'
).requires(
    'impact.entity'
)
.defines(function (){

    EntityHotspot = ig.Entity.extend({

        type: null,
        keyItem: null,
        isVisited: false,
        visible: true,
        isNewTouch:true,

        init: function( x, y, settings ) {
            if ( this.animSheet ) {
                this.addAnim( 'idle', 0.01, [0,2,4] );
                this.addAnim( 'opened', 0.01, [1,3,5] );
            }
            this.parent( x, y, settings );
        },

        activate: function () {
            this.isVisited = true;

            if (this.anims.opened)
                this.currentAnim = this.anims.opened.rewind();

            return this.type;
        },

        draw: function() {
            if (this.visible) {
                this.parent();
            }
        }

    });
});