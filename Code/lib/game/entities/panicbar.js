



ig.module(
    'game.entities.panicbar'
)
.requires(
    'impact.entity'
)
.defines(function (){

    EntityPanicbar = ig.Entity.extend({

        strokeStyle:'rgb( 249, 190, 32 )',
        fillStyle:'rgb( 255, 0, 0 )',
        player: null,
        textWidthLeft: 50,
        textWidthRight: 50,
        textDistance:25,
        fontMedium: new ig.Font( 'media/fonts/font_lucida_fax_italic.png' ),

        init: function( x, y, settings ) {
            this.parent( x, y, settings );
            this.size = { x: ig.system.width-(2*this.textDistance),  y: 30};
            this.pos = { x : this.textDistance, y : 5 };
        },

        update: function() {

        },

        draw: function() {

            if (this.player)
            {

                var ctx = ig.system.context;

                if(this.player.currentScareState == Scarestate.HIGH) {
                    this.fillStyle = 'rgb(255,0,0)';
                    //this.strokeStyle = 'rgb(255,0,0)';
                } else if(this.player.currentScareState == Scarestate.MEDIUM) {
                    this.fillStyle = 'rgb(251,130,0)';
                    //this.strokeStyle = 'rgb(251,130,0)';
                } else {
                    this.fillStyle = 'rgb(0,141,240)';
                    //this.strokeStyle = 'rgb(0,141,240)';
                }


                ctx.globalAlpha = (this.player.panicLevel/this.player.maxPanicLevel * 0.9) + 0.1;
                ctx.fillStyle = this.fillStyle;
                // panic box
                var panic = this.player.panicLevel;
                var diff = 5;
                var width = (this.size.x-(this.textWidthLeft+this.textWidthRight+2*this.textDistance+2*diff))*panic/this.player.maxPanicLevel;
                ctx.fillRect( this.pos.x+this.textWidthLeft+this.textDistance+diff, this.pos.y+diff, width, this.size.y-2*diff);

                ctx.globalAlpha = 0.8;


                ctx.strokeStyle = this.strokeStyle;
                ctx.lineWidth = 3;
                ctx.strokeRect(this.pos.x+this.textWidthLeft+this.textDistance, this.pos.y, this.size.x-(this.textWidthRight+this.textWidthLeft+2*this.textDistance), this.size.y);

                ctx.globalAlpha = 1.0;

                this.fontMedium.draw('CALM', this.textDistance, this.pos.y + 7, ig.Font.ALIGN.LEFT);
                this.fontMedium.draw('PANIC', ig.system.width - this.textDistance  - this.textWidthRight, this.pos.y + 7, ig.Font.ALIGN.LEFT);

            }
        }

    });


});