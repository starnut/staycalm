/**
 * Created by Michel Wacker on 26.01.13
 * Copyright by Starnut, all rights reserved.
 */
ig.module(
    'game.entities.player'
)
.requires(
    'impact.entity',
    'game.scarestate',
    'game.playerstate'
)
.defines(function(){

    EntityPlayer = ig.Entity.extend({

        collides: ig.Entity.COLLIDES.ACTIVE,
        type: ig.Entity.TYPE.A,
        checkAgainst: ig.Entity.TYPE.B,

        fillStyle:'rgb( 255, 255, 255 )',
        fillAlpha: 0.5,

        currentScareState: null,
        lightRadius: 0,
        lightOffset: {x: 0, y: -20},

        moveSpeed: 0,
        runSpeed: 0,

        panicFactor: 1,
        panicLevel: 0,
        maxPanicLevel: 100,
        minPanicLevel: 10,

        fireTimer: 0,

        inventory: null,
        isLighterBurning: false,
        lighterTimer: new ig.Timer(),
        ligherBurnDuration: 5,
        lighterTextIndex: -1,

        timerHeartbeat: null,
        lastHeartbeat: null,
        size: {x:64, y:100},
        offset: {x:32, y:50},
        animSheet: new ig.AnimationSheet( 'media/entities/char.png', 64, 100 ),

        //strokeStyle:'rgb( 138, 197, 62 )',
        //lineWidth: 2,

        init: function( x, y, settings ) {

            this.addAnim( 'idle', 1, [6] );
            this.addAnim( 'moveUp', 0.2, [9,10,11] );
            this.addAnim( 'moveDown', 0.2, [6,7,8] );
            this.addAnim( 'moveLeft', 0.2, [3,4,5] );
            this.addAnim( 'moveRight', 0.2, [0,1,2] );



            this.parent( x, y, settings );

            this.inventory = [];

            this.setPlayerState( Playerstate.ALIVE );
            this.setScareState( this.currentScareState );

            this.timerHeartbeat = new ig.Timer();
            this.lastHeartbeat = this.timerHeartbeat.delta();
        },

        update: function() {
            this.parent();

            if ( this.currentPlayerState == Playerstate.DEAD ) {
                return;
            }

            var speed = ( ig.input.state( 'moveFast' )) ? this.runSpeed : this.moveSpeed;
            speed = speed * ig.system.tick;

            // check for movement
            var x = this.pos.x,
                y = this.pos.y,
                halfWidth = this.size.x / 2,
                halfHeight = this.size.y / 2,
                ani;
            if ( ig.input.state( 'moveUp' ) ){
                y -= speed;
                ani = this.anims.moveUp;
            }
            else if ( ig.input.state( 'moveDown' ) ) {
                y += speed;
                ani = this.anims.moveDown;
            }

            if ( ig.input.state( 'moveLeft' ) ){
                x -= speed;
                ani = this.anims.moveLeft;
            }
            else if ( ig.input.state( 'moveRight' ) ) {
                x += speed;
                ani = this.anims.moveRight;
            }
            // delimit movement
            if ( x < halfWidth ) {
                x = halfWidth;
            }
            else if ( x > ig.system.width - halfWidth ){
                x = ig.system.width - halfWidth;
            }

            if ( y < halfHeight ) {
                y = halfHeight;
            }
            else if ( y > ig.system.height - halfHeight ){
                y = ig.system.height - halfHeight;
            }

            this.pos.x = x;
            this.pos.y = y;

            if (!ani)  {
                ani = this.anims.idle;
            }
            // set animation
            if (this.currentAnim != ani )
                this.currentAnim = ani;


            this.panicLevel = this.panicLevel + this.panicFactor * ig.system.tick;
            this.panicLevel = ( this.panicLevel < this.minPanicLevel ) ? this.minPanicLevel : this.panicLevel;

            this.checkPanicLevel();

            // play heart beat as sounds
            var diff = this.timerHeartbeat.delta() - this.lastHeartbeat;
            var threshold = 30 / this.panicLevel;
            threshold = Math.max(threshold, 0.13);
            //threshold = 0.13;
            if (diff > threshold)
            {
                //var d = new Date();
                //console.log(diff.toFixed(2) + " " + threshold.toFixed(3) + ' time ='+ d.getSeconds()+'.'+ d.getMilliseconds());

                // play heart beat
                this.lastHeartbeat = this.timerHeartbeat.delta();
                Config.playSoundFile(Config.SOUND.HEARTBEAT);
                //console.log('player heartbeat with diff=' + diff + ' and ' + this.panicLevel);
            }

            // check ligher timer
            if (this.isLighterBurning && this.lighterTimer.delta() > this.ligherBurnDuration) {
                this.extinguishLighter();
            }
        },

//        draw: function() {
//            this.parent();
//        },


        //------------------------------
        // API

        enterTheLight: function() {
            this.setScareState( Scarestate.LOW );
        },

        leaveTheLight: function() {
            if ( this.currentScareState == Scarestate.INIT ) {
                return;
            }
            if ( this.isLighterBurning ) {
                this.setScareState( Scarestate.MEDIUM );
            }
            else {
                this.setScareState( Scarestate.HIGH );
            }
        },

        jumpScare: function( value ) {
            this.panicLevel += value;

            this.checkPanicLevel();
        },

        addItemToInventory: function( item ) {
            var index = this.getItemIndex( item.type );
            if ( index > -1 ) {
                this.inventory[index].amount++;
            }
            else {
                this.inventory.push( item );
            }
            this.listInventory();
        },

        getItemIndex: function( type ) {
            var n = this.inventory.length;
            for ( var i = 0; i < n; i++ ) {
                if ( this.inventory[i].type == type ) {
                    return i;
                }
            }
            return -1;
        },

        removeItemFromInventory: function( type ) {
            var index = this.getItemIndex( type );
            if ( index > -1 ) {
                var item = this.inventory[index];

                if ( item.amount > 0 ) {
                    item.amount--;
                    if ( item.amount <= 0 ) {
                        this.inventory.splice( index, 1 );
                    }
                    return true;
                }
            }
            return false;
        },

        listInventory: function() {
            var n = this.inventory.length;
            for ( var i = 0; i < n; i++ ) {
                console.log( this.inventory[i].type + " " + this.inventory[i].amount );
            }
        },

        //------------------------------
        // METHODS

        setPlayerState: function( state ) {
            switch ( state ) {
                case Playerstate.ALIVE:
                {
                    this.currentAnim = this.anims.idle;
                    break;
                }
                case Playerstate.DEAD:
                {
                    this.currentAnim = this.anims.idle;
                    this.isLighterBurning = false;
                    break;
                }
                default:
                {
                    throw("Failed to set invalid player state: " + state);
                    return;
                }
            }
            this.currentPlayerState = state;
        },

        activateLighter: function() {
            if ( this.isLighterBurning ){
                return;
            }
            if ( Math.random() > 0.30 ) {
                Config.playSoundFile(Config.SOUND.ZIP);
                this.setScareState( Scarestate.HIGH );
                return false;
            }
            this.isLighterBurning = true;
            this.setScareState( Scarestate.MEDIUM );

            Config.playSoundFile(Config.SOUND.CANDLE);


            //set timer
            this.ligherBurnDuration = 10 + Math.floor(Math.random()*10);
            this.lighterTimer.set(0);
            return true;
        },

        extinguishLighter: function() {
            this.isLighterBurning = false;
            this.lighterTextIndex++;
            this.lighterTextIndex%3;
            if(this.lighterTextIndex == 0) {
                ig.game.textlighter.playTextBlock(ig.game.text.textSystem.fuckingLigher1);
            } else if(this.lighterTextIndex == 1) {
                ig.game.textlighter.playTextBlock(ig.game.text.textSystem.fuckingLigher2);
            } else {
                ig.game.textlighter.playTextBlock(ig.game.text.textSystem.fuckingLigher3);
            }

        },

        checkPanicLevel: function() {
            if ( this.panicLevel <= 0 ) {
                this.panicLevel = 0;
            }
            else if ( this.panicLevel >= this.maxPanicLevel ) {
                this.setPlayerState( Playerstate.DEAD );
                Config.playSoundFile(Config.SOUND.DEATH);
                ig.game.setGameMode(Gamemode.OVER);
            }
        },

        setScareState: function( state ) {

            switch ( state ) {
                case Scarestate.INIT:
                {
                    this.lightRadius = 0;
                    this.panicFactor = Config.PANIC_FACTOR.INIT;
                    break;
                }
                case Scarestate.HIGH:
                {
                    this.lightRadius = 0;
                    this.panicFactor = Config.PANIC_FACTOR.HIGH;
                    break;
                }
                case Scarestate.MEDIUM:
                {
                    this.lightRadius = Config.PLAYER.LIGHT_RADIUS;
                    this.panicFactor = Config.PANIC_FACTOR.MEDIUM;
                    break;
                }
                case Scarestate.LOW:
                {
                    this.lightRadius = Config.PLAYER.LIGHT_RADIUS;
                    this.panicFactor = Config.PANIC_FACTOR.LOW;
                    break;
                }
                default:
                {
                    throw("Failed to set invalid scare state: " + state);
                    return;
                }
            }
            this.currentScareState = state;
        },

        touches: function( other ) {
            return !(
                this.pos.x - this.offset.x >= other.pos.x + other.size.x ||
                    this.pos.x + this.size.x - this.offset.x<= other.pos.x ||
                    this.pos.y - this.offset.y >= other.pos.y + other.size.y ||
                    this.pos.y + this.size.y - this.offset.y <= other.pos.y
                );
        }
    });
});