/**
 * Created by Michel Wacker on 26.01.13
 * Copyright by Starnut, all rights reserved.
 */
ig.module(
    'game.entities.table'
)
.requires(
    'game.entities.internal.hotspot'
)
.defines(function(){

    EntityTable = EntityHotspot.extend({
        size: { x: 124,  y: 58 },
        animSheet: new ig.AnimationSheet( 'media/entities/table.png', 124, 58 )
    });
});