


ig.module(
    'game.entities.text'
)
.requires(
    'impact.entity'
)
.defines(function (){

    EntityText = ig.Entity.extend({

        font: new ig.Font( 'media/fonts/font_lucida_fax_italic.png' ),

        soundFile: null,
        soundsPlayed: null,

        textUnit: null,
        textIndex: null,

        player: null,

        isDone: null,

        textSystem: {
            empty: {
               blocks: ['']
            },

            introductionLighter: {
                blocks: [
                    'Hello?', ' Hello!?',' Where am I?',{sound: 'doorslam'},null,
                    'Who is there? ', 'Hello?!',null,
                    'It\'s so dark here, ','I need light...', null,
                    'What\'s that? ','A lighter!',{sound: 'zipopen'},null,
                    '(Press F to use lighter)'
                ]
            },
            // trigger this after pressing F
            introductionMovement: {
                blocks: [
                    'This place gives me the creeps.',null,
                    'It feels like...',' the darkness is alive.',null,
                    'God, ','I\'m so scared!',' (Watch your panic bar)', null,
                    'I need more light to calm down.', null,
                    '(Move with W,A,S,D - press SPACE to use items)', null
                ]
            },
            eventCandlePickedUp: {
                blocks: [{sound: 'candlepick'},
                    'Six candles! ',null,
                    'They should help lighten up this place.',null,
                    'But where would I place them?', null
                ]
            },
            eventCandleHolder: {
                blocks: [
                    'Much better. The light is soothing.',null,
                    'I\'ll stay close for a while and calm down.',null,
                    'But I\'ve got to find a way out...', null
                ]
            },
            introductionGetOutOfHere: {
                blocks: [
                    'Oh my god, this place is horrible. ',null,
                    'I need to get out of here.',null//,
                    //'Maybe this door is the exit.'
                ]
            },
            eventExitWithNoKeys: {
                blocks: [
                    {sound: 'doorklinke'},'Shit it\'s locked!',null,
                    'There must be a key somewhere...',null,
                    {sound: 'doorslam'},'Woah! What was that? Shit!',null,
                    'Let me OUT!',null,
                    'Oh my God, this place is horrible.',null,
                    'I need to get out of here.',null,
                    'Maybe this door is the exit.',null
                ]
            },

//            // Candles
//            eventCandlePickedUp: {
//                blocks: [
//                    {sound: 'candlepick'},
//                    'Ahh more light!',null
//                ]
//            },
            eventCandlesOutOfCandles: {
                blocks: [
                    'Oh no, I\'m out of candles', null,
                    'I\'m running out of time!',null
                ]
            },

            // Table
            eventTableIsEmpty: {
                blocks: [
                    'There is nothing useful here.',null
                ]
            },

            eventTableFoundKey: {
                blocks: [
                    {sound: 'key'},
                    'A key?!... could it be that easy?.',null
                ]
            },

            // Wardrobe
            eventWardrobeLocked: {
                blocks: [
                    {sound: 'chestlocked'},
                    'I can\'t open it. It\'s locked.',null
                ]
            },
            eventWardrobeFoundAxe: {
                blocks: [
                    {sound: 'closetopen'},
                    'Let\'s see... Why is there an axe in the closet?',null,
                    'But it may prove useful.',null
                ]
            },
            eventWardrobeIsEmpty: {
                blocks: [
                    'Nothing useful inside...',null
                ]
            },


            // Chest
            eventChestIsLocked: {
                blocks: [
                    {sound: 'chestlocked'},
                    'I need something to open it.',null
                ]
            },
            eventChestDestroyedFoundKey:{
                blocks: [
                    'Let\'s crack this crappy chest open.',
                    {sound: 'chestopen'},null,
                    'Another key?','Finally, I hope it fits.',null
                ]
            },
            eventChestIsEmpty: {
                blocks: [
                    'Wow, I really messed it up.',null
                ]
            },

            // Exit
            eventExitWithNoKeysSecond: {
                blocks : [
                    {sound: 'doorklinke'},
                    'I have to find the key...',null
                ]
            },
            eventExitWonGame: {
                blocks: [
                    'I\'m free!',null
                ]
            },
            fuckingLigher1: {
                blocks: [
                    'Fucking lighter!',null
                ]
            },
            fuckingLigher2: {
                blocks: [
                    'Crap...',null
                ]
            },
            fuckingLigher3: {
                blocks: [
                    '%&$ˆ*! It\'s hot!',null
                ]
            }
        },

        init: function( x, y, settings ) {
            this.isDone = true;
        },

        playTextBlock: function (textBlock) {
            //var date =  new Date();
            //console.log('playTextBlock = '+date.getSeconds() + '.' + date.getMilliseconds());

            // set current text block
            this.textUnit = textBlock;
            this.textIndex = 0;

            this.isDone = false;

            // calc position
            var x = this.textUnit.x;
            var y = this.textUnit.y;
            if (x==null && this.player!=null)
            {
                x = this.player.pos.x - Config.PLAYER.LIGHT_RADIUS/2;
                y = this.player.pos.y + Config.PLAYER.LIGHT_RADIUS + 10;
            }
            x = Math.max(x,40);
            y = Math.max(y,40);
            this.textUnit.x = Math.min(Config.SETUP.WIDTH-400,x);
            this.textUnit.y = Math.min(Config.SETUP.HEIGHT-200,y);

            this.soundsPlayed = 0;
            // start timer
            this.timer = new ig.Timer();
            this.timer.set(0);
        },


        update: function() {
            if (this.textUnit == null)
            {
                return;
            }

            this.textIndex = 1 + this.soundsPlayed + Math.floor(this.timer.delta()*1.25);

            if (this.soundFile != null)
            {
                // play sound
                this.playSoundFile(this.soundFile);
                this.soundsPlayed++;

                //console.log('this.soundsPlayed='+this.soundsPlayed);

                // reset sound
                this.soundFile = null;
            }
        },

        playSoundFile: function (name)
        {
            //console.log('play sound = ' + name);
            var sound = new ig.Sound( 'media/sounds/'+name+'.*' );
            sound.play();
        },

        draw: function() {
            if (this.textUnit == null)
            {
                return;
            }
            var currentLineText = '';
            var offsetY = 0;

            var x = this.textUnit.x;
            var y = this.textUnit.y;

            for (var i = 0; i < this.textIndex; i++) {

                if (i >= this.textUnit.blocks.length)
                {
                    if (!this.isDone)
                    {
                        this.isDone = true;
                        ig.game.textSystemIsDone(this.textUnit);
                    }
                    break;

                }

                var item = this.textUnit.blocks[i];

                if (item == null) {

                    // draw current line
                    this.font.draw(currentLineText, x, y + offsetY, ig.Font.ALIGN.LEFT);

                    // make new line
                    offsetY += 35;
                    currentLineText = '';
                }
                else if (typeof item == 'string')
                {
                    currentLineText += item;
                }
                else
                {
                    // play sound
                    this.soundFile = item.sound;
                    this.textUnit.blocks[i] = '';
                }
            }
            // draw last line
            this.font.draw(currentLineText, x, y + offsetY, ig.Font.ALIGN.LEFT);
        }
    });


});