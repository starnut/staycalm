


ig.module(
    'game.entities.textlighter'
)
    .requires(
    'impact.entity'
)
    .defines(function (){

        EntityTextlighter = ig.Entity.extend({

            font: new ig.Font( 'media/fonts/font_lucida_fax_italic.png' ),

            soundFile: null,
            soundsPlayed: null,

            textUnit: null,
            textIndex: null,

            player: null,

            isDone: null,

            init: function( x, y, settings ) {
                this.isDone = true;
            },

            playTextBlock: function (textBlock) {
                //var date =  new Date();
                //console.log('playTextBlock = '+date.getSeconds() + '.' + date.getMilliseconds());

                // set current text block
                this.textUnit = textBlock;
                this.textIndex = 0;

                this.isDone = false;

                // calc position
                var x = this.textUnit.x;
                var y = this.textUnit.y;
                if (x==null && this.player!=null)
                {
                    x = this.player.pos.x - Config.PLAYER.LIGHT_RADIUS/2;
                    y = this.player.pos.y - Config.PLAYER.LIGHT_RADIUS + 10;
                }
                x = Math.max(x,40);
                y = Math.max(y,40);
                this.textUnit.x = Math.min(Config.SETUP.WIDTH-400,x);
                this.textUnit.y = Math.min(Config.SETUP.HEIGHT-200,y);

                this.soundsPlayed = 0;
                // start timer
                this.timer = new ig.Timer();
                this.timer.set(0);
            },


            update: function() {
                if (this.textUnit == null)
                {
                    return;
                }

                this.textIndex = 1 + this.soundsPlayed + Math.floor(this.timer.delta());

                if (this.soundFile != null)
                {
                    // play sound
                    this.playSoundFile(this.soundFile);
                    this.soundsPlayed++;

                    //console.log('this.soundsPlayed='+this.soundsPlayed);

                    // reset sound
                    this.soundFile = null;
                }
            },

            playSoundFile: function (name)
            {
                //console.log('play sound = ' + name);
                var sound = new ig.Sound( 'media/sounds/'+name+'.*' );
                sound.play();
            },

            draw: function() {
                if (this.textUnit == null)
                {
                    return;
                }
                var currentLineText = '';
                var offsetY = 0;

                var x = this.textUnit.x;
                var y = this.textUnit.y;

                for (var i = 0; i < this.textIndex; i++) {

                    if (i >= this.textUnit.blocks.length)
                    {
                        if (!this.isDone)
                        {
                            this.isDone = true;
                            ig.game.textSystemIsDone(this.textUnit);
                        }
                        break;

                    }

                    var item = this.textUnit.blocks[i];

                    if (item == null) {

                        // draw current line
                        this.font.draw(currentLineText, x, y + offsetY, ig.Font.ALIGN.LEFT);

                        // make new line
                        offsetY += 35;
                        currentLineText = '';
                    }
                    else if (typeof item == 'string')
                    {
                        currentLineText += item;
                    }
                    else
                    {
                        // play sound
                        this.soundFile = item.sound;
                        this.textUnit.blocks[i] = '';
                    }
                }
                // draw last line
                this.font.draw(currentLineText, x, y + offsetY, ig.Font.ALIGN.LEFT);
            }
        });


    });