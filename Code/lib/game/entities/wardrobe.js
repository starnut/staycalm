/**
 * Created by Michel Wacker on 26.01.13
 * Copyright by Starnut, all rights reserved.
 */
ig.module(
    'game.entities.wardrobe'
)
.requires(
    'game.entities.internal.hotspot'
)
.defines(function(){

    EntityWardrobe = EntityHotspot.extend({
        size: { x: 119,  y: 122 },
        animSheet: new ig.AnimationSheet( 'media/entities/wardrobe.png', 119, 122 )
    });
});