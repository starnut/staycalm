/**
 * Created by Michel Wacker on 16.08.12
 * Copyright by Starnut, all rights reserved.
 */
ig.module(
    'game.file'
)
.defines(function(){ //"use strict";

    File = {
        FONT: {
            //FUNDS: 'media/fonts/funds.font.png',
        },

        ENTITIES: {
            //CAR: 'media/entities/car.png',
        },

        MAPS: {
            LEVEL1: 'media/maps/level1.jpg'
        },

        UI: {
            //BADGE_ABORT: 'media/ui/badge-abort.png'
        },

        SOUND: {
            //AMBIENCE: 'media/sounds/ambience.mp3',
            //BUTTON: 'media/sounds/button.mp3'
        }
    }
});