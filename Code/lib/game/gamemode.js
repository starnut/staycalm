/**
 * Created by Michel Wacker on 26.01.13
 * Copyright by Starnut, all rights reserved.
 */
ig.module(
    'game.gamemode'
)
.defines(function(){ //"use strict";

    Gamemode = {
        INIT: "init",
        RUNNING: "running",
        OVER: "over",
        COMPLETE: "complete"
    }
});