ig.module( 
	'game.main' 
)
.requires(
	'impact.game',
	'impact.font',
    'game.config',
    'game.gamemode',
    'game.entities.player',
    'game.entities.panicbar',
    'game.entities.internal.hotspot',
    'game.entities.text',
    'game.entities.textlighter',
    'game.entities.candle',
    'game.entities.controls',
    'game.levels.test'
)
.defines(function(){

MyGame = ig.Game.extend({

    fontMedium: new ig.Font( 'media/fonts/font_lucida_fax_italic.png' ),
    player: null,
    candles: null,
    //items: null,
    hotspots: null,
    panicBar: null,
    gameMode: null,
    isCandleIntroductionShown: false,
    //controlsHud: null,

    text: null,
    textlighter: null,

    introductionMovementFinished: false,
    introductionMovementCleared: false,
    fireIntroductionPlayed: false,

    debug: false,

	init: function() {
		// Initialize your game here; bind keys etc.

        this.setGameMode( Gamemode.INIT );
    },

    loadLevel: function( data ) {
        this.parent( data );
    },

    setGameMode: function( mode ) {
        this.gameMode = mode;

        switch ( mode ) {
            case Gamemode.INIT:
            {
                isCandleIntroductionShown = false;
                introductionMovementFinished = false;
                introductionMovementCleared = false;
                fireIntroductionPlayed = false;

                Config.ITEMS.candles.amount = 6;

                ig.input.unbindAll();

                ig.input.bind( ig.KEY.SPACE, 'use' );
                ig.input.bind( ig.KEY.ENTER, 'restart' );

                this.loadLevelDeferred( LevelTest );
                break;
            }
            case Gamemode.RUNNING:
            {
                this.startGame();
                break;
            }
            case Gamemode.OVER:
            {
                //TODO: this.loadLevelDeferred(LevelCredits);
                break;
            }
        }
    },

    setupGame: function() {
        var x = ig.system.width/2,
            y = ig.system.height/2;

        // spawn player
        this.player = this.spawnEntity(
            EntityPlayer,
            x, y,
            Config.PLAYER.SETTINGS
        );

        // panic bar
        this.panicBar = this.spawnEntity(
            EntityPanicbar,
            10, //Config.UI.PANIC_BAR.POS.x,
            10,//Config.UI.PANIC_BAR.POS.y,
            { player: this.player } // Config.UI.PANIC_BAR.SETTINGS
        );

        // controlsHud view
//        this.controlsHud = this.spawnEntity(
//            EntityControls,
//            256,
//            256,
//            {}
//        );

        this.text = this.spawnEntity(EntityText);
        this.text.player = this.player;
        this.text.playTextBlock( this.text.textSystem.introductionLighter);

        this.textlighter = this.spawnEntity(EntityTextlighter);
        this.textlighter.player = this.player;

        ig.music.add( 'media/sounds/ambient.* ' );
        ig.music.loop = true;
        ig.music.play();
        // get candles from map
        this.candles = this.getEntitiesByType( EntityCandle );
        // get items from map
        //this.items = this.getEntitiesByType( EntityItem );
        // get items from map
        this.hotspots = this.getEntitiesByType( EntityHotspot );

        var positions = [];
        for (var i = 0; i < this.hotspots.length; i++)
        {
            var hotspot = this.hotspots[i];
            if (hotspot.type != 'candles' && hotspot.type != 'exit')
            {
                positions.push(hotspot.pos);
            }
        }
        this.shuffle(positions);

        for (var i = 0; i < this.hotspots.length; i++)
        {
            var hotspot = this.hotspots[i];
            if (hotspot.type != 'candles' && hotspot.type != 'exit')
            {
                hotspot.pos = positions.pop();
            }
        }

    },

    shuffle: function(array){
        for (var i = array.length - 1; i > 0; i--) {
            var j = Math.floor(Math.random() * (i + 1));
            var tmp = array[i];
            array[i] = array[j];
            array[j] = tmp;
        }
    },

    textSystemIsDone: function (textBlock) {

        if (textBlock == this.text.textSystem.introductionLighter)
        {
            ig.input.bind( ig.KEY.F, 'fire' );
            ig.input.bind( ig.KEY.W, 'moveUp' );
            ig.input.bind( ig.KEY.S, 'moveDown' );
            ig.input.bind( ig.KEY.A, 'moveLeft' );
            ig.input.bind( ig.KEY.D, 'moveRight' );
            ig.input.bind( ig.KEY.SHIFT, 'moveFast' );
        }
        else if (textBlock == this.text.textSystem.introductionMovement)
        {
            this.introductionMovementFinished = true;
        }
        else if (textBlock == this.text.textSystem.eventCandlePickedUp ||
            textBlock == this.text.textSystem.eventCandleHolder ||
            textBlock == this.text.textSystem.eventCandlesOutOfCandles ||
            textBlock == this.text.textSystem.eventTableFoundKey ||
            textBlock == this.text.textSystem.eventTableIsEmpty ||
            textBlock == this.text.textSystem.eventChestDestroyedFoundKey ||
            textBlock == this.text.textSystem.eventChestIsEmpty ||
            textBlock == this.text.textSystem.eventChestIsLocked ||
            textBlock == this.text.textSystem.eventWardrobeFoundAxe ||
            textBlock == this.text.textSystem.eventWardrobeIsEmpty ||
            textBlock == this.text.textSystem.eventWardrobeLocked ||
            textBlock == this.text.textSystem.eventExitWithNoKeys ||
            textBlock == this.text.textSystem.eventExitWithNoKeysSecond
            )
        {
            this.text.playTextBlock(this.text.textSystem.empty);
        }
        else if (textBlock == this.text.textSystem.fuckingLigher1 ||
            textBlock == this.text.textSystem.fuckingLigher2 ||
            textBlock == this.text.textSystem.fuckingLigher3)
        {
            this.textlighter.playTextBlock(this.text.textSystem.empty);
        }
        //console.log('text clip done');
    },

    startGame: function() {
        /*
        if (this.candles) {
            for (var i = 0; i < this.candles.length; i++) {
                var candle = this.candles[i];
                candle.burn();
            }
        }
         */
    },



//    randomPosition: function(min, range){
//        return min + Math.floor(Math.random()*range);
//    },
	update: function() {
		// Update all entities and backgroundMaps
		this.parent();

        if (this.gameMode == Gamemode.INIT)
        {
            if (ig.input.pressed( 'use' ))
            {
                this.setupGame();
                this.setGameMode( Gamemode.RUNNING );
            }
        }

        if ( this.gameMode == Gamemode.RUNNING ) {

            if (ig.input.pressed( 'fire' )) {
                if ( !this.player.isLighterBurning ) {
                    if ( this.player.activateLighter() &&
                        !this.fireIntroductionPlayed ) {
                        this.fireIntroductionPlayed = true;
                        this.text.playTextBlock( this.text.textSystem.introductionMovement);
                    }
                }
            }

            if (!this.introductionMovementCleared && this.introductionMovementFinished && (ig.input.state( 'moveUp' ) || ig.input.state( 'moveDown' ) || ig.input.state( 'moveLeft' ) || ig.input.state( 'moveRight' ))) {
                this.text.playTextBlock( this.text.textSystem.empty);
                 this.introductionMovementCleared = true;
            }
            // check whether a player touches a burning candle
            var i;
            var n;
            if ( this.candles ) {
                var isPlayerInLight = false;

                n = this.candles.length;
                for ( i = n; --i >= 0; ) {
                    var candle = this.candles[i];
                    if ( candle.isBurning &&
                        this.player.distanceTo( candle ) <= candle.currentRadius ) {
                        isPlayerInLight = true;
                    }
                    else {
//                        if (  this.player.removeItemFromInventory( 'candles' )) {
//                            console.log("bla")
//                        }

                        if ( ig.input.pressed( 'use' ) &&
                             this.player.touches( candle ) &&
                             candle.currentRadius < candle.lightRadius / 2) {

                            if(candle.isNewTouch) {
                                candle.isNewTouch = false;
                                if(this.player.removeItemFromInventory( 'candles' ) ) {
                                    candle.refill();
                                    candle.burn();
                                } else {
                                    this.text.playTextBlock(this.text.textSystem.eventCandlesOutOfCandles);
                                }
                            }
                        } else {
                            candle.isNewTouch = true;
                        }
                    }
                }
                //
                if (isPlayerInLight) {
                    this.player.enterTheLight();
                } else {
                    this.player.leaveTheLight();
                }
            }

//            if ( this.items ) {
//                n = this.items.length;
//                for ( i = n; --i >= 0; ) {
//                    var item = this.items[i];
//                    if ( this.player.touches( item ) ) {
//                        this.player.addItemToInventory( item );
//                        this.items.splice( i, 1 );
//
//                        //item.visible = false;
//                    }
//                }
//            }

            if ( ! this.player.killed && this.hotspots && ig.input.pressed( 'use' ) ) {
                n = this.hotspots.length;
                for ( i = n; --i >= 0; ) {
                    var hotspot = this.hotspots[i];
                    if ( this.player.touches( hotspot )) {

                        if (hotspot.isNewTouch) {
                            if ( hotspot.isVisited) {
                                // text: this is empty
                                // note candles ?!
                                // note exit does not need be handled because player already won
                                if (hotspot.type == 'table')
                                {
                                    this.text.playTextBlock(this.text.textSystem.eventTableIsEmpty);
                                }
                                else if(hotspot.type == 'wardrobe')
                                {
                                    this.text.playTextBlock(this.text.textSystem.eventWardrobeIsEmpty);
                                }
                                else if (hotspot.type == 'chest')
                                {
                                    this.text.playTextBlock(this.text.textSystem.eventChestIsEmpty);
                                }
                            }
                            else {
                                if ( !hotspot.keyItem ||
                                     this.player.getItemIndex( hotspot.keyItem ) > -1 ) {

                                    hotspot.activate();
                                    this.getItemFromHotspot( hotspot.type );
                                }
                                else {
                                    //  text: I cannot open this yet

                                    // note candles ?!
                                    // note on the table the key can always be picked up
                                    if (hotspot.type == 'wardrobe')
                                    {
                                        this.text.playTextBlock(this.text.textSystem.eventWardrobeLocked);
                                    }
                                    else if (hotspot.type == 'chest')
                                    {
                                        this.text.playTextBlock(this.text.textSystem.eventChestIsLocked);
                                    }
                                    else if (hotspot.type == 'exit')
                                    {
                                        this.text.playTextBlock(this.text.textSystem.eventExitWithNoKeys);
                                    }
                                }
                            }
                            hotspot.isNewTouch = false;
                        }
                    }else {
                        hotspot.isNewTouch = true;
                    }
                }
            }
        }


        if (this.gameMode == Gamemode.COMPLETE || this.gameMode == Gamemode.OVER)
        {
            if (ig.input.pressed( 'restart' ))
            {
                this.setGameMode(Gamemode.INIT);
            }
        }

	},

    getItemFromHotspot: function( type ) {
        var itemType;

        // successful event you did / found something useful

        switch ( type ) {
            case 'candles':
            {
                itemType = 'candles';
                this.text.playTextBlock(this.text.textSystem.eventCandlePickedUp);
                break;
            }
            case 'table':
            {
                itemType = 'key';
                ig.game.text.playTextBlock(ig.game.text.textSystem.eventTableFoundKey);
                break;
            }
            case 'wardrobe':
            {
                ig.game.text.playTextBlock(ig.game.text.textSystem.eventWardrobeFoundAxe);
                itemType = 'axe';
                break;
            }
            case 'chest':
            {
                ig.game.text.playTextBlock(ig.game.text.textSystem.eventChestDestroyedFoundKey);
                itemType = 'doorKey';
                break;
            }
            case 'exit':
            {
                this.setGameMode( Gamemode.COMPLETE );
                this.player.kill();
                Config.playSoundFile(Config.SOUND.CRASH);
                break;
            }
        }
        if ( itemType ) {
            this.player.addItemToInventory( Config.ITEMS[itemType] );
        }
    },
	
	draw: function() {
        if ( this.gameMode == Gamemode.INIT ) {
            new ig.Image( 'media/screens/start_screen.jpg' ).draw( 0, 0 );
            new ig.Image( 'media/entities/controls.png' ).draw( ig.system.width-250-30, ig.system.height-211-10 );
            this.fontMedium.draw('Press SPACE to start', Config.SETUP.WIDTH / 2, 600, ig.Font.ALIGN.CENTER);
            return;

        }

        if (this.gameMode == Gamemode.RUNNING)
        {
            if (!this.debug)
            {
                this.startClipping();
            }

            // Draw all entities and backgroundMaps
            this.parent();
        }

        if (this.gameMode == Gamemode.COMPLETE || this.gameMode == Gamemode.OVER)
        {
            new ig.Image( 'media/maps/background.jpg' ).draw( 0, 0 );

            if (this.gameMode == Gamemode.COMPLETE)
            {
                new ig.Image( 'media/screens/victory_screen.png' ).draw( 0, 0 );
            }
            else
            {
                new ig.Image( 'media/screens/gameover_screen.png' ).draw( 190, 40 );
            }
            var y = 500;
            var diff = 20;

            if (this.gameMode == Gamemode.OVER)
            {
                var description = 'You died, because this game scared the living crap out of you.';
                this.fontMedium.draw(description, Config.SETUP.WIDTH / 2, y, ig.Font.ALIGN.CENTER);
            }

            y+= diff;
            y+= diff;
            y+= diff;

            this.fontMedium.draw('Artist', Config.SETUP.WIDTH / 2 - 30, y, ig.Font.ALIGN.RIGHT);
            this.fontMedium.draw('Florian Smolka', Config.SETUP.WIDTH / 2 + 30, y, ig.Font.ALIGN.LEFT);

            y+= diff;
            y+= diff;
            this.fontMedium.draw('Code', Config.SETUP.WIDTH / 2 - 30, y, ig.Font.ALIGN.RIGHT);
            this.fontMedium.draw('Lukas Lang', Config.SETUP.WIDTH / 2 + 30, y, ig.Font.ALIGN.LEFT);
            y+= diff;
            this.fontMedium.draw('Michel Wacker', Config.SETUP.WIDTH / 2 + 30, y, ig.Font.ALIGN.LEFT);
            y+= diff;
            this.fontMedium.draw('Michael Kugler', Config.SETUP.WIDTH / 2 + 30, y, ig.Font.ALIGN.LEFT);

            y+= diff;
            y+= diff;

            this.fontMedium.draw('Sound & Story Writing', Config.SETUP.WIDTH / 2 - 30, y, ig.Font.ALIGN.RIGHT);
            this.fontMedium.draw('Michael Niederreiter', Config.SETUP.WIDTH / 2 + 30, y, ig.Font.ALIGN.LEFT);



            this.fontMedium.draw('Press ENTER to restart', Config.SETUP.WIDTH - 30, Config.SETUP.HEIGHT - 30, ig.Font.ALIGN.RIGHT);


        }

        // candle
        //this.font.draw('this.health = '+ this.candle.health.toFixed(0), 50,50,ig.Font.ALIGN.LEFT);
	},

    startClipping: function () {
        if ( !this.candles || !this.player ) {
            return;
        }
        //draw clipping mask
        var ctx = ig.system.context;
        ctx.save();

        ctx.fillStyle = 'rgb(0,0,0)';
        ctx.fillRect( 0, 0, ig.system.width, ig.system.height );
        ctx.beginPath();
        var clipLightSources = false;
        if ( this.candles ) {
            for (var i = 0; i < this.candles.length; i++) {
                var candle = this.candles[i];
                if ( candle.isBurning && candle.currentRadius > 0 ) {
                    clipLightSources = true;
                    ctx.moveTo(candle.pos.x + candle.clippingOffset.x, candle.pos.y + candle.clippingOffset.y);
                    ctx.arc(
                        candle.pos.x + candle.clippingOffset.x,
                        candle.pos.y + candle.clippingOffset.y,
                        this.calcLightVariance( candle.currentRadius ),
                        0, 2*Math.PI, false
                    );
                }
            }
        }
        //
        if( this.player.isLighterBurning ) {
            clipLightSources = true;

            ctx.moveTo(this.player.pos.x + this.player.lightOffset.x, this.player.pos.y + this.player.lightOffset.y);
            ctx.arc(
                this.player.pos.x + this.player.lightOffset.x,
                this.player.pos.y + this.player.lightOffset.y,
                this.calcLightVariance( this.player.lightRadius ),
                0, 2*Math.PI, false
            );
        }
        // if nothing was drawn we need something small to make sure the clipping works
        if (!clipLightSources) {
             ctx.rect(0,0,1,1);
        }

        ctx.closePath();
        ctx.clip();
    },

    calcLightVariance: function ( radius ) {
        radius += 2 * ( Math.random() + 0.5 );
        radius = ( radius < 0 ) ? 0 : radius;
        return radius;
    },

    stopClipping: function () {
        var ctx = ig.system.context;
        ctx.restore();
    },

    drawEntities: function() {
        if ( this.gameMode == Gamemode.INIT ) {
            return;
        }

        if (this.gameMode == Gamemode.RUNNING) {
            var i;
            for( i = 0; i < this.candles.length; i++ ) {
                this.candles[i].draw();
            }

            for( i = 0; i < this.hotspots.length; i++ ) {
                this.hotspots[i].draw();
            }
            this.player.draw();

            if (!this.debug)
            {
                this.stopClipping();
            }

            this.panicBar.draw();
            //this.controlsHud.draw();
            this.text.draw();
            this.textlighter.draw();
        }


    }

//    initSound: function( name, loops ) {
//        if ( !ig.SM2 ) {
//            return;
//        }
//        if ( !ig.SM2.getSoundById( name ) ) {
//            loops = loops || 1;
//            var snd = ig.SM2.createSound({
//                id: name,
//                url: Util.getSoundURL( name ),
//                loops: loops,
//                autoLoad: true,
//                stream: false,
//                multiShot: ! ig.ua.mobile
//            });
//            return snd;
//        }
//    }


});


    // Start the Game with 60fps, a resolution and a scale factor
    ig.main( '#canvas', MyGame, Config.SETUP.FPS, Config.SETUP.WIDTH, Config.SETUP.HEIGHT, Config.SETUP.SCALE );

});
