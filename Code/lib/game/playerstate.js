ig.module(
    'game.playerstate'
)
.defines(function(){ //"use strict";

    Playerstate = {
        ALIVE: 'playerStateAlive',
        DEAD: 'playerStateDead'
    };
});