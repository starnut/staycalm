ig.module(
    'game.scarestate'
)
    .defines(function(){ //"use strict";

        Scarestate = {
            HIGH: 'scareStateHigh',
            MEDIUM: 'scareStateMedium',
            INIT: 'scareStateInit',
            LOW: 'scareStateLow'
        };
    });