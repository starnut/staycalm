// original source: https://gist.github.com/1395616
ig.module(
    'plugins.button'
)
.requires(
    'impact.entity'
)
.defines(function() {
    Button = ig.Entity.extend({
        size: {x: 40, y: 40 },          // public

        view: null,                     // private
        viewSrc: null,                  // setting
        viewOffset: 0,                  // setting

        text: [],                       // setting
        textPos: { x: 5, y: 5 },        // setting
        textAlign: ig.Font.ALIGN.LEFT,  // setting

        font: null,                     // private
        fontSrc: null,                  // setting

        state: null,                    // private
        stateCounter: 0,                // private
        buttonStates: null,             // setting


        selectable: false,              // setting
        selected: false,                // read only
        selectedOffset: 1,              // setting

        enabled: true,                  // read only
        visible: true,                  // public

        wasOver: false,                 // private

        init: function( x, y, settings ) {
            this.parent( x, y, settings );

            if ( !this.animSheet && !this.viewSrc ) {
                throw ("Failed to create button: Set 'viewSrc' property!");
            }

            this.animSheet = this.animSheet || new ig.AnimationSheet(
                this.viewSrc,
                this.size.x,
                this.size.y
            );

            // init animations for available button states and set init state
            this.buttonStates = this.buttonStates || [Button.STATE.UP];
            this.updateStateAnims( this.buttonStates );

            this.setState ( this.state || Button.STATE.UP );

            // disable if initialised
//            if ( settings.enabled === false ||
//                 this.state === Button.STATE.DISABLED
//                ) {
//                this.setEnabled( false );
//            }

            // text property has been set. attempt to initialise font
            if ( this.text.length > 0 && this.font === null ) {
                this.font = ( this.fontSrc ) ? new ig.Font( this.fontSrc ) : ig.game.font;

                if ( !this.font ) {
                    throw ("Failed to initialise font in button. Set global font or button property 'fontSrc'!");
                }
            }
        },

        update: function() {
            if ( !this.visible || !this.enabled ){
                return;
            }

            var isPressed = ig.input.state( 'click' ),
                over = this.isOver(),
                newState;

            // release button but only trigger function when over
            if ( ig.input.released( 'click' ) ) {
                if ( this.state !== Button.STATE.UP ) {
                    newState = Button.STATE.UP;
                    if ( over ) {
                        this.onUp();
                    }
                }
            }

            if ( over ) {
                // override up state with over state if a release happened while over
                if ( newState ) {
                    newState = Button.STATE.OVER;
                } else if ( !this.wasOver ) {
                    // mouse rolls in
                    this.wasOver = true;
                    // switch to over state only if button has been released while over
                    if ( !isPressed ) {
                        newState = Button.STATE.OVER;
                    }
                    this.onOver();
                }

                // clicks may only happen while over the button
                if ( ig.input.pressed( 'click' ) ) {
                    newState = Button.STATE.DOWN;
                    this.onDown();
                }

            } else {
                // mouse rolls out
                if ( this.wasOver ) {
                    this.wasOver = false;
                    // switch to up state only if button has been released while over
                    if ( !isPressed ) {
                        newState = Button.STATE.UP;
                    }
                    this.onOut();
                }
            }
            // only update state if it was changed
            if ( newState && newState !== this.state ) {
                this.setState( newState );
            }
        },


        draw: function() {
            //ig.log("visible " + this.visible );
            if ( !this.visible ) {
                return;
            }
            // prevent scrolling: override parent draw function
            if( this.currentAnim ) {
                this.currentAnim.draw(
                    this.pos.x - this.offset.x,
                    this.pos.y - this.offset.y
                );
            }

            var i;
            for ( i = 0; i < this.text.length; i++ ) {
                this.font.draw(
                    this.text[i],
                    this.pos.x + this.textPos.x,// - ig.game.screen.x,
                    this.pos.y + ((this.font.height + 2) * i) + this.textPos.y,// - ig.game.screen.y,
                    this.textAlign
                );
            }
        },


        updateStateAnims: function (states) {
            this.stateCounter = 0;
            var i;
            for (i = 0; i < states.length; i++) {
                this.updateStateAnim( states[i] );
            }
        },

        updateStateAnim: function ( name ) {
            this.updateAnimation( name, 1, [this.viewOffset + this.stateCounter] );
            this.stateCounter += 1;
        },

        updateAnimation: function ( name, timePerFrame, sequence ) {
            var anim = this.anims[name];
            if ( !anim ) {
                anim = this.addAnim( name, timePerFrame, sequence );
            } else {
                anim.sequence = sequence;
                anim.tile = sequence[0];
            }
            return anim;
        },


        setState: function( state ) {
            if ( !state ) {
                return;
            }
            this.currentAnim = this.anims[ this.validateState( state ) ];
            // store originally passed state to allow control and comparison
            this.state = state;
        },

        validateState: function( state ) {
            if ( this.anims[ state ] ) {
                return state;
            }
            switch ( state ) {
                case Button.STATE.UP:
                    throw ("Failed to set 'up' state in button! Check config and tile sheet!");

                case Button.STATE.DOWN:
                    return this.validateState ( Button.STATE.OVER );

                default: // over and disabled default to UP
                    return this.validateState ( Button.STATE.UP );
            }
        },


        /*isOver: function() {
            return  ig.input.mouse.x + ig.game.screen.x > this.pos.x &&
                ig.input.mouse.x + ig.game.screen.x < this.pos.x + this.size.x &&
                ig.input.mouse.y + ig.game.screen.y > this.pos.y &&
                ig.input.mouse.y + ig.game.screen.y < this.pos.y + this.size.y;
        }, */
        isOver: function() {
            return  ig.input.mouse.x > this.pos.x &&
                ig.input.mouse.x < this.pos.x + this.size.x &&
                ig.input.mouse.y > this.pos.y &&
                ig.input.mouse.y < this.pos.y + this.size.y;
        },


        onDown: function() {},//{ ig.log("down"); },

        onOut:function() {},//{ ig.log("out"); },

        onOver:function() {},//{ ig.log("over"); },

        onUp: function() {
            //ig.log("up");
            this.setSelected( !this.selected );
        },

        setSelected: function( selected ) {
            if ( !this.selectable || this.selected === selected ) {
                return;
            }
            var o = this.viewOffset;
            this.viewOffset = this.selectedOffset;
            this.selectedOffset = o;

            this.selected = selected;

            this.updateStateAnims( this.buttonStates );
        },

        setEnabled: function ( enabled ) {
            if ( !enabled ) {
                this.setState( Button.STATE.DISABLED );
            } else {
                this.setState( Button.STATE.UP);
            }
            this.enabled = enabled;
        }
    });


    Button.STATE = {
        UP: 'up',
        OVER: 'over',
        DOWN: 'down',
        DISABLED: 'disabled'
    };
});