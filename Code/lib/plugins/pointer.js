ig.module(
    'plugins.pointer'
)
.requires(
    'impact.entity'
)
/**
 * A 1x1 pointer, following the mouse. On a mouse click it triggers
 * the 'onClick' function (if available) in any entity it currently collides with i.e.
 * the mouse is currently over.
 */
.defines(function(){
    Pointer = ig.Entity.extend({

        //checkAgainst: ig.Entity.TYPE.B,
        size: {x:1, y:1},
        //isClicking: false,

        update: function() {
            this.pos.x = ig.input.mouse.x + ig.game.screen.x;
            this.pos.y = ig.input.mouse.y + ig.game.screen.y;
            //this.isClicking = ig.input.pressed('click');
        }/*,

        check: function( other ) {
            // the user is clicking and the entity this collides with has
            // a 'clicked' function
            if ( this.isClicking && typeof(other.onClick) === 'function' ) {
                other.onClick();
            }
        }*/
    });
});