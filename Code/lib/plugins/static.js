/**
 * Created by Michel Wacker on 30.10.12
 * Copyright by Starnut, all rights reserved.
 */
ig.module(
    'plugins.static'
)
.requires(
    'impact.entity'
)
.defines(function(){

    EntityStatic = ig.Entity.extend({

        visible: true,

        init: function ( x, y, settings ) {
            this.parent( x, y, settings );

            this.addAnim( 'idle', 1, [0] );
        },

        draw: function () {
            if ( this.visible ) {
                this.parent();
            }
        }
    });
});