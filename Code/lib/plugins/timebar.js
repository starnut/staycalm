ig.module(
    'plugins.timebar'
)
.requires(
    'impact.entity'
)
.defines(function() {
    Timebar = ig.Entity.extend({

        size: {x:450, y:25},

        //fontTime: new ig.Font( Config.MEDIA.FONT_TIME ),
        //textPos: null,

        total: 0,
        delta: 0,
        ratio: 0,
        drawOffset: 0,
        drawTarget: null,

        view: null,

        init: function ( x, y, settings ) {
            this.parent( x, y, settings );
            this.setTime( this.total, this.delta );

            this.drawTarget = {
                x: this.pos.x - this.offset.x,// - ig.game._rscreen.x,
                y: this.pos.y - this.offset.y// - ig.game._rscreen.y
            };
            /*this.textPos = {
                x: this.pos.x + Config.UI.TIMER.POS_TEXT.x,
                y: this.pos.y + Config.UI.TIMER.POS_TEXT.y
            };  */
        },


        setTime: function ( total, delta ) {
            this.total = total;
            this.delta = delta;
            this.ratio = this.delta / this.total;
            this.ratio = ( this.ratio > 1 ) ? 1 : this.ratio;
            // do not round here. may cause flickering gfx
            this.drawOffset =  Math.floor( this.size.x * this.ratio );
        },


        draw: function() {
            var h = this.size.y,
                w = this.size.x;

            //ig.log( this.ratio );
            if ( this.ratio <= 0 ) {
                this.view.draw(
                    this.drawTarget.x,
                    this.drawTarget.y,
                    0, h, w, h
                );

            } else if ( this.ratio >= 1 ) {
                this.view.draw(
                    this.drawTarget.x,
                    this.drawTarget.y,
                    0, 0, w, h
                );

            } else {
                this.view.draw(
                    this.drawTarget.x + w - this.drawOffset,
                    this.drawTarget.y,
                    w - this.drawOffset, 0, this.drawOffset, h
                );

                this.view.draw(
                    this.drawTarget.x,
                    this.drawTarget.y,
                    0, h, w - this.drawOffset, h
                );
            }

            /*this.fontTime.draw(
                Math.ceil( this.total - this.delta ) + 's',
                this.textPos.x,
                this.textPos.y,
                ig.Font.ALIGN.RIGHT
            );*/
        }
    });
});